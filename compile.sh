xelatex -synctex=1 -interaction=nonstopmode ConquaireBook

cd ch1-Introduction
xelatex -synctex=1 -interaction=nonstopmode ch1-Introduction
bibtex ch1-Introduction

cd ../ch-Technical-Description
xelatex -synctex=1 -interaction=nonstopmode ch-Technical-Description
bibtex ch-Technical-Description

cd ../ch2-BiologyDuerr
xelatex -synctex=1 -interaction=nonstopmode ch2-BiologyDuerr
bibtex ch2-BiologyDuerr

cd ../ch3-BiologyEgelhaaf
xelatex -synctex=1 -interaction=nonstopmode ch3-BiologyEgelhaaf
bibtex ch3-BiologyEgelhaaf

cd ../ch4-ChemistryKoop
xelatex -synctex=1 -interaction=nonstopmode ch4-ChemistryKoop
bibtex ch4-ChemistryKoop

cd ../ch5-EconomicsHoog
xelatex -synctex=1 -interaction=nonstopmode ch5-EconomicsHoog
bibtex ch5-EconomicsHoog

cd ../ch6-LinguisticsRohlfing
xelatex -synctex=1 -interaction=nonstopmode ch6-LinguisticsRohlfing
bibtex ch6-LinguisticsRohlfing

cd ../ch7-LinguisticsSchlangen
xelatex -synctex=1 -interaction=nonstopmode ch7-LinguisticsSchlangen
bibtex ch7-LinguisticsSchlangen

cd ../ch8-PsychologySchneider
xelatex -synctex=1 -interaction=nonstopmode ch8-PsychologySchneider
bibtex ch8-PsychologySchneider

cd ../ch9-TechnologyWachsmuth
xelatex -synctex=1 -interaction=nonstopmode ch9-TechnologyWachsmuth
bibtex ch9-TechnologyWachsmuth

cd ../ch10-Conclusion
xelatex -synctex=1 -interaction=nonstopmode ch10-Conclusion
bibtex ch10-Conclusion

cd ..

xelatex -synctex=1 -interaction=nonstopmode ConquaireBook
xelatex -synctex=1 -interaction=nonstopmode ConquaireBook
