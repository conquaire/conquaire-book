


\chapter{Preface}
%\addcontentsline{toc}{chapter}{Preface}
        
This book is a direct result of the \emph{Conquaire} (Continuous Quality Control for Research Data to Ensure Reproducibility) project, which was funded by the DFG between 2016 and 2019.
The goal of the project was to understand in how far principles from continuous quality control and test-driven development as nowadays being state-of-the-art in software engineering can be applied to the management of research data to increase its quality and potential for re-use.

In order to arrive at such an understanding, we have been closely working together with researchers from different disciplines at Bielefeld University ranging from biology, over chemistry, economics, linguistics, psychology through to computer science / robotics.
All in all, we have been working with eight research groups and have defined a case study in reproducibility with each of these groups. Within these use cases we have aimed at reproducing one central part of a previously published research article.
In doing this, we have limited ourselves to reproducing the computational analysis leading to the particular result, as reproducing the actual experiments would have been outside of the scope of the Conquaire project.

The book that lies in front of you documents these eight case studies and describes what we have done to reproduce the specific results. In most cases, reproducing the analytical result, in spite of data and scripts being available, has only been possible by close interaction and guidance by the authors of the original publication, which in all cases are direct co-authors of the chapter describing our reproducibility experiments.

The work conducted in the case studies has provided us with a detailed understanding of the analytical workflows used by all the case study partners and has allowed us to get a deep understanding of barriers and challenges in reproducing published results. Thus, we can give a number of clear recommendations at the end of the book, representing the lessons learned from the practical attempt to reproduce a number of published results.

This exercise in understanding requirements and problems for analytical reproducibility would not have been possible without funding by DFG. Most critically, it would not have been possible without the effort and dedication of the eight research groups we have worked with for the last three years. We would like to thank all of them for their patience with us and for bearing with us while walking on the sometimes stony path of achieving reproducibility. We thank all of the research groups for providing us data, scripts, describing their workflows, etc. All of these groups have been engaged in this project because they were interested in finding how to improve their workflows to make their results transparent, reproducible and thus better accessible to the scientific community. We thank all of these groups for engaging in the project in spite of the risk that comes with a higher level of transparency and exposure. By being transparent, one risks that others can discover some flaws in the way things have been done. This is quite a risk in science, a risk that nevertheless we have to take as progress in science should always be weighted higher than the consequences for particular individuals.         

We would like to thank all the student researchers involved in Conquaire who have supported the activities of reproducing results. We would like to thank in particular Lukas Biermann and Fabian Herrmann as they have been central to the success of many case studies, having worked day-to-day with many of the above mentioned research groups and having developed central pieces of the Conquaire infrastructure for supporting continuous quality control of research data.
We would also like to thank Vidya Ayer, who has been working on the project since its start. She has been key in pulling together the different chapters that this book consists of and provided a very early draft version of a manuscript for the book.
Finally, we would like to thank John P. McCrae for contributing to the Conquaire project proposal. Many of the key ideas of Conquaire go back to him.

It has been a pleasure and very rewarding to work with all these scientists and learning about their very specific research questions, goals, and methods. We hope that you find this book as exciting to read as it was for us to edit it. \\
\vspace{0.5cm} \\
Bielefeld, 29th September 2019 \\
\vspace{0.5cm} \\
Philipp Cimiano, Christian Pietsch, Cord Wiljes
        
%        Before you lies the technical research report “The Conquaire Book: Case Studies in Computational Reproducibility!”, undertaken as part of the Conquaire research studies at Bielefeld University. Conquaire was a 3-year DFG-funded project and this book has been written to provide a detailed report on our computational reproducibility research findings conducted with our partners during the course of building the generic git-based technical infrastructure that would interface with the existing institutional repository for research data management (RDM) at Bielefeld University. 
        
%        As the primary researcher working on the Conquaire research project, I was engaged in creating the quality checker for CSV documents, researching computational reproducibility for each of our case study partners and writing this book report from February 2016 to January 2019. My research focus was in the area of computational reproducibility for research while ensuring data quality, a difficult topic as it was based on a study of diverse interdisciplinary research groups who collaborated as our case study partners but conducting this extensive investigation has given me insights into finding solutions for questions that we identified; while providing an excellent opportunity to write about the research work for two publications, a recent CODATA publication and the earlier D-Lib Magazine article; alongside the opportunity to speak and present at various public conferences, symposiums, plenaries and workshops.
        
%        This book documents the full research journey, from infrastructure to analytical reproducibility studies, that showcase the successful results alongside the challenges faced and most importantly, the findings during the course of my research work which can roughly be summarized into two main goals in writing this technical report:
        
%        \begin{itemize}
%            \item \textbf{A structured approach:} Managing research artefacts cannot be solved by throwing technology at the problem and the readers of this book will grasp the fact that the technical issues need to be complemented with a structured approach; viz. how to manage research metadata alongside ensuring data quality, educating researchers about good data management practices and providing technical training, adopting tools on par with industry standards, etc.
%            \item \textbf{Learnings from our partners and peers:} For a computational reproducibility study it was important to study their current scenario and learn from the partner researchers about their existing data management practices. For effective RDM practices, we adopted the FAIR data principles and our case studies have been analysed on the basis of these data management facets which will provide the readers a good insight into translatable RDM practices.
%        \end{itemize}  
%        
%        
%        %SS-1.1
%        \section{Contents} \label{toc}
%        %SS-1.1
%        
%        The Introduction chapter-1 provides a foundation and motivation for the research focus of Conquaire, introduces the research groups and provides a taxonomy for grading the reproducibility experiments outcomes. The Chapters 2–9 describe each reproducibility case study undertaken on a past publication, their research, our successes, the challenges, and the abilities to effectively manage their own research data. Despite the extensive research on each group, there is no blueprint or cookie-cutter practice for group success in research data management as each research field has diverse requirements and what works in one group situation may fail in another. Hence, armed with specific FAIR data principles, standards, procedures, and other feedback techniques provided in this book, the researchers can make more informed choices and should be able to facilitate and structure their group’s data management in conjunction with the generic infrastructure being built to help and support their group. 
%        
%        
%        %SS-2
%        \section{ACKNOWLEDGEMENTS} \label{ack}
%        %SS-2
%        
%        Writing a technical research report in a book form is a long process but a rewarding outcome nevertheless. I would like to express my gratitude to all the people who supported and guided me in the process of bringing this book to life during this 3-year process. 
%        
%        I take this opportunity to thank my supervisor Philipp Cimiano for his guidance during the book discussions, the Conquaire team and wish to thank all the project partners who participated in Conquaire, without whose contribution I would not have been able to conduct this research. A special round of thanks for everyone who collaborated in the Conquaire reproducibility experiments, or contributed and helped co-author the individual case study chapters, namely:
%        
%        \begin{itemize}
%            \item \textit{chapter1-Introduction:} Philipp Cimiano, Vitali Piel, Dirk Pieper, Christian Pietsch, Jochen Schirrwagen and Cord Wiljes.
%            \item \textit{chapter2-BiologyDuerr:} Volker Dürr, Yannick Günzel, Lukas Biermann and Fabian Herrmann.
%            \item \textit{chapter3-BiologyEgelhaaf:} Olivier Bertrand, Vineet Sharma, Jens Lindemann, Cord Wiljes, Martin Egelhaaf and Philipp Cimiano.
%            \item \textit{chapter4-ChemistryKoop:} Thomas Koop, Evelyn Jantsch, and Fabian Herrmann.
%            \item \textit{chapter5-EconomicsHoog:} Sander van der Hoog and Krishna Devkota for writing the FlaViz library.
%            \item \textit{chapter6-LinguisticsRohlfing:} Katharina Rohlfing, Iris Nomikou and Lukas Biermann.
%            \item \textit{chapter7-LinguisticsSchlangen:} Julian Hough and David Schlangen.
%            \item \textit{chapter8-PsychologySchneider:} Rebecca Foerster and Werner Schneider
%            \item \textit{chapter9-TechnologyWachsmuth:} Florian Lier, Phillip L{\"u}cking, Sebastian Meyer zu Borgsen, Sven Wachsmuth, Jasmin Bernotat, Friderieke Eyssel, Robert Goldstone, Selma \v{S}abanovi\'{c}
%    \end{itemize} 
%    
%    
%    I enjoyed speaking and presenting at various European conferences, symposiums, plenaries and workshops in the areas of open science, library communities, and research data management summer school for the lively discussions and learning opportunities from my peers. Being from the libre software communities, most peers were from known circles and these events provided a good opportunity to catch up with old friends, meet new peers and make new connections. Despite the online communication tools, being able to talk directly to people whose work you admire is a unique experience - it gave me the chance to ask questions, volley ideas and discuss technical issues common to research institutions and listen to their experiences and observe what technical solutions was being implemented at their institutions.
%    
%    This book would not be complete without acknowledging my family, my parents who deserve my deepest gratitude for their infinite motivation, support, wisdom, trust and for always being there; Thomas, for his patience and support; and all my friends whose kindness provided me strength over the years. Thank you!
%    
%    I hope you enjoy reading this book as I did writing it!
%    
%    %\begin{authorsignoff}
%    %    \Author{Vidya Ayer\\ 
%    %        \mailto{vid@svaksha.com}}
%    %\end{authorsignoff}
%    
%    
%    
%    14-January-2019, Bielefeld, Germany.
    

