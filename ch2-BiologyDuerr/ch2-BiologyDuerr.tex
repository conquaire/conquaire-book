\chapter{Reproducibility of whole-body movement analyses of insects}

\label{duerr_chapter}

\chapterauthor[1]{Yannick Günzel}
\chapterauthor[2]{Fabian Hermann}
\chapterauthor[2]{Vidya Ayer}
\chapterauthor[2]{Philipp Cimiano}
\chapterauthor[1]{Volker Dürr}
\begin{affils}
     \chapteraffil[1]{Biological Cybernetics, Faculty of Biology, Bielefeld University}
     \chapteraffil[2]{Semantic Computing Group, Faculty of Technology \& Cognitive Interaction Technology Excellence Center (CITEC), Bielefeld University}
\end{affils}
    
 
\section*{Abstract} \label{abstract}
In this chapter we describe the attempt to reproduce a selected figure of the paper \emph{``Comparative whole-body kinematics of closely related insect species with different body morphology''} \cite{Theunissen_EtAl_2015}. In this paper, the authors investigated the walking behaviour of three different species of stick insects. This was done by recording whole-body kinematics of the animals, using a commercial marker-based motion capture system and custom written MATLAB scripts. The main objective of the study was to relate inter-species differences in kinematics to differences in overall morphology, including features such as leg-to-body-length ratio that were not an obvious result of phylogenetic or ecological divergence. The present chapter describes an effort to reproduce one of the figures that was published in the original study that evaluates climbing behaviour by means of (i) snapshots of body posture, (ii) 3D trajectories of front legs and body, and (iii) the gait pattern of a representative trial. We show that the figure could be reproduced successfully, albeit requiring detailed interaction with the authors as well as use of commercial software. Accordingly, we classify this use case as corresponding to our category limited analytical reproducibility. The data and scripts are available in the following Git repository: \url{https://gitlab.ub.uni-bielefeld.de/conquaire/biological-cybernetics}.


\section*{Keywords} \label{keywords} 
Insect Locomotion, Whole-body kinematics, MATLAB


\section{Introduction} \label{intro_duerr}    

The overall goal of the Biological Cybernetics lab at Bielefeld University is to understand the mechanisms underlying the control of natural movement and action sequences. To this end, the lab studies the adaptive locomotion abilities of insects with a research focus on the function of active tactile sensing (touch) and distributed proprioception (the sense of posture). A key methodology of the lab is whole-body motion capture of unrestrained walking and climbing insects (e.g., \citep{Theunissen_Duerr_2013} \citep{Theunissen_EtAl_2015}), which was also in the focus of the present data management study. More recently, whole-body motion capture has been combined with ground-reaction force measurements and the corresponding calculation of single-joint torques \citep{Dallmann_EtAl_2016}, as well as coincident muscle activity recordings during unrestrained walking \citep{Dallmann_EtAl_2017}.  
Insects have become important model animals for the study of flexible and adaptive locomotion (e.g., \citep{Ritzmann_Bueschges_2007} \citep{Duerr_EtAl_2018}). Although a wide range of behavioural (e.g., \citep{Cruse_EtAl_2009}), biomechanical (e.g., \citep{Full_EtAl_1991}) and neurophysiological (\citep{Burrows_1996}, \citep{Bueschges_2012}) studies on insect locomotion have contributed to a detailed understanding of multi-legged locomotion in general, there are very few studies on comparative kinematics of insect walking or climbing.     
Legged locomotion through natural or naturalistic environments is very complex and variable. Leg kinematics may not only differ strongly among species, but also within the same species it is adaptive and context-dependent. Inter-species differences in locomotion are often difficult to interpret, because both morphological and ecological differences among species may be strong and, as a consequence, confound each other's effects. Moreover, in species from phylogenetically distant taxa, i.e., that diverged a long time ago in evolution, differences in motor behaviour may simply be a result of evolutionarily divergent morphological or physiological constraints. 
The experimental data of the present case study was taken from a study that is to date the only example of a whole-body kinematics comparison of different insect species \citep{Theunissen_EtAl_2015}. The species compared differed in body morphology, despite close phylogenetic relationship and similar ecology. \textit{Carausius morosus}, \textit{Aretaon asperrimus} and \textit{Medauroidea extradentata} (= \textit{Cuniculina impigra}) belong to the same order of insects (\textit{Phasmatodea}: stick and leaf insects). All three species are flightless and live a herbivorous and nocturnal life style. 
Accordingly, the main objective of that study was to relate inter-species differences in kinematics to differences in overall morphology, including features such as leg-to-body-length ratio, that were not an obvious result of phylogenetic or ecological divergence. The original study suggests that major differences among species were related to antenna length, segment lengths of thorax and head, and the ratio of leg length over body length. 

    
    
    %S-3
    \section{Methods} \label{duerr_methods}
    %S-3
    This section describes the material and methods used in the research project published in \citep{Theunissen_EtAl_2015}. After illustrating the overall workflow (subsection \ref{datLW}), we describe the acquisition of the original experimental data (subsection \ref{expProc}), the manual editing and annotation procedures (subsection \ref{dataAnnotation}), as well as the secondary data processing (subsection \ref{secondDataP}). Note that subsections \ref{expProc} to \ref{secondDataP} repeat previously published method section parts of \citep{Theunissen_Duerr_2013} and \citep{Theunissen_EtAl_2015}.
    
    
    %SS-3.1
    \subsection{Data workflow: acquisition and processing pipeline} \label{datLW}
    %SS-3.1
    
    The overall data workflow used in this project is summarized in the chart shown in Fig. \ref{fig:fig2-workflow} (left column). There were three processing episodes: (i) data acquisition, (ii) manual editing and annotation, and (iii) secondary processing. The coloured boxes illustrate the procedure for recording the different types of data and how it was ultimately processed to reconstruct body and leg kinematics as displayed in Fig. 3 in the paper of Theunissen et al. \cite{Theunissen_EtAl_2015}. The colours of the boxes indicate the software used for a given step in the data processing pipeline (yellow: \textit{Vicon Nexus}; green: \textit{PixeLINK Capture}; blue: \textit{MATLAB}). The boxes and connecting arrows are labelled with the data file types produced, the relative file paths to the corresponding subdirectories, and the names of custom-written MATLAB (MathWorks, Natick, MA, USA) scripts.
    
    
\begin{figure}[]
  \centering
  \includegraphics[width=11cm,keepaspectratio]{images/fig2-Workflow.png}  
  \caption{\textbf{Research data acquisition and processing pipeline.} For raw data acquisition, whole body motions were recorded with a marker-based motion capture system (Vicon) and an additional digital video camera. Furthermore, the anatomy of the animal, along with the marker positions on different body segments were recorded with a microscope camera. In a first step of manual editing and annotation, marker trajectories of selected episodes were labelled and, potentially, connected in case of recording gaps. This step resulted in a \textit{.c3d}-file, a file format described in section \ref{c3dServerIO}. The body pictures were used to generate a body model containing, for example, segment lengths and information about marker position in a body-centred coordinate system. The model is stored in a MATLAB \textit{.mat}-file. Finally, the kinematic reconstruction was achieved in MATLAB by combining marker trajectories with the body documentation. The resulting processed data, i.e., joint angle time courses, gait pattern, and velocity were saved as another MATLAB file.}
  \label{fig:fig2-workflow}
\end{figure}

    
    
    %SS-3.2
    \subsection{Data acquisition: Experimental procedure} \label{expProc}
    %SS-3.2
    For the experiments described in \citep{Theunissen_EtAl_2015}, adult stick insects of the species \textit{Carausius morosus} (de Sinéty 1901), \textit{Aretaon asperrimus} (Brunner von Wattenwyl 1907) and \textit{Medauroidea extradentata} (Redtenbacher 1906) were used. Animals were bred in a laboratory culture at Bielefeld University. 
    
    In each experimental trial, an animal was placed on a horizontal walkway (40 x 490 mm), along which it walked freely. There were four walking/climbing conditions as characterised by the height of two stairs placed on the walkway: in the flat (walking) condition, the walkway was used without stairs; in the climbing conditions low, middle and high, a staircase with two stairs of step height, h, was placed at the end of the walkway (40 x 200 mm; low: h = 8 mm, middle: h = 24 mm, high: h = 48 mm). The flat walking condition served as the reference condition. The four conditions were presented in a randomised sequence of at least 40 trials, resulting in approximately ten trials per condition per animal. The whole setup was painted in opaque black and was surrounded by black drapery in order to minimise visual contrast. The room was darkened and illuminated only by red light LEDs of the Vicon cameras (see below) and indirect light emanating from a TFT computer monitor. 
    
    A marker-based motion capture technique was applied, for which each animal was labelled by 17 or 18 retro-reflective markers (Fig. \ref{fig:body_kinematics}). Marker diameter was 1.5 mm. Markers were glued to the cuticle by use of transparent nail polish. Two markers were attached to each leg, one to the distal femur and one to the distal tibia (Fig. \ref{fig:body_kinematics}, right panel). Additionally, five markers were attached to thorax and head, with three markers defining the body-fixed coordinate system of the metathorax and one additional marker on the prothorax and head (Fig. \ref{fig:body_kinematics} B, left panel). In most animals, a further marker was placed on the rostral mesothorax. Care was taken that neither the nail polish nor the markers constrained the movement of any joint. Segment dimensions and the positions of all markers on their respective body segment were measured from high-resolution photographs (0.02 mm per pixel) taken under a stereo lens (Olympus SZ61T, equipped with a digital camera (Pixelink PL-B681CU), controlled by \textit{$\mu$Scope} software (top right green box in Fig. \ref{fig:fig2-workflow}).
    
    A Vicon MX10 motion capture system with eight \emph{T10 cameras} (Vicon, Oxford, UK) was used for data acquisition of marker positions (top yellow box in Fig. 2). Temporal resolution of the motion capture system was 200 Hz; spatial resolution was approximately 0.1 mm. The time of entry of the animal into the capture volume was used as starting frame of the recording. The recording was stopped when the animal reached the far end of the setup. Trials were discarded if the animal climbed the side walls of the setup instead of the stairs, or stopped walking before the first stair. In this case, the same trial condition was repeated. 
    
    An additional digital video camera (Basler A602fc, Ahrensburg, Germany) equipped with a near range zoom lens (Edmund Optics, Barrington, NJ, USA) was used to record a complementary image sequence for visual inspection, e.g., for validation of the kinematic analysis. The video showed a side view of the climbing sequence of the first stair, with a temporal resolution of 50 Hz (synchronized with the Vicon system) and a spatial resolution of approximately 0.14 mm per pixel. The software Nexus 1.4.1 (Vicon, Oxford, UK) was used for controlling the motion capture process and for subsequent offline analysis. 
    
    
    %SS-3.3
    \subsection[Manual editing and annotation]{Manual editing and annotation: trajectory labelling and body model} \label{dataAnnotation}
    %SS-3.3
    
    Within Nexus, each of the markers was identified and labelled at least once per trial by hand (second yellow box in Fig. \ref{fig:fig2-workflow}). Markers were then tracked automatically, provided that each marker was recorded by at least two cameras. The resulting trajectories of spatial coordinates of all markers were inspected for filling of small trajectory gaps. Generally, marker detection was very robust. On average, less than 5 gaps per 60 s occurred in single marker trajectories in case of \textit{C. morosus} trials, with mean trial durations of 11.29 $\pm$ 4.8 s, equivalent to 2258 $\pm$ 964 frames (mean $\pm$ standard deviation). Gaps shorter than 200 ms (40 frames) were filled by use of an interpolation algorithm of the software Nexus.
   
    
    A body model was established for each animal, using a custom-written Graphical User Interface in MATLAB (top right blue box in \ref{fig:fig2-workflow}) that loaded all available photos and prompted the user to click on segment limits and marker locations (middle blue box in \ref{fig:fig2-workflow}). The body model consisted of a branched kinematic chain (Fig. \ref{fig:body_kinematics}B) with a four-segmented body axis and six three-segmented limbs. The corresponding body model file contains information about body and leg segment dimensions, attachment locations of side chains on the main chain (i.e., the locations of the thorax-coxa joints), the marker coordinates relative to the base of their carrying segment, and bias rotations of the marker-fixed coordinate system defined by the three makers on the root segment (Fig. \ref{fig:body_kinematics}B, left panel) relative to the body-centred coordinate system defining the sagittal, horizontal and transverse planes of the body.
    
    
    %SS-3.4
    \subsection{Secondary processing: Whole-body kinematics} \label{secondDataP}
    %SS-3.4
    
    Whole-body kinematics yielded the joint angle time courses associated with 42 degrees of freedom (DoF) of the body model. All calculations were done in MATLAB (lower blue box in \ref{fig:fig2-workflow}), using the toolbox C3Dserver (Motion Analysis Laboratory, Erie, PA, USA), for importing C3D data from Vicon Nexus (sub-section \ref{c3dServerIO}).
    
    
    
\begin{figure}[]
  \centering
  \includegraphics{./images/fig3-MotionCaptureBodyKinematics.jpg}
  \caption{\textbf{A marker-based motion capture and whole-body kinematics calculations.} \textbf{A:} Insects were labelled with reflective markers. \textbf{B:} For kinematic analysis, the body was modelled by a branched kinematic chain. The main body chain (left) consists of the three thorax segments (Root, T2, T1) and the head. Six side chains (right) model the legs, with the segments coxa, femur and tibia (cox, fem, tib; only right legs are shown, labelled R1 to R3). All rotation axes (DoF) are indicated (3 for the root segment, 2 for thorax/head segments, and 5 per leg). DoF are denoted according to the subsequent segment and the axis of the local coordinate system around which the rotation is executed. Leg DoF are: cox.x, cox.y, cox.z (labelled for R2 in right panel), fem.y and tib.y (labeled for R1 in right panel). [Fig. 1 A, B of \citep{Theunissen_Duerr_2013}]}
  \label{fig:body_kinematics}
\end{figure}
    
  
    \textit{Scaling and filtering}: Joint angles were calculated by use of two data sets coming from (i) the segment lengths and marker positions on the animal, as calibrated under the stereo lens, and (ii) the marker trajectories, as obtained from motion-capturing. 
    
    Since the body model measurements were more precise than the Vicon calibration, the marker trajectories were scaled by the factor lBM/lMC, where lBM is the distance of two markers in the body model with fixed distance (e.g., two markers on the metathorax), and lMC is the corresponding mean distance of the same markers in the motion capture data. lBM/lMC ranged from 0.94 to 1.00, mainly depending on the calibration quality of the Vicon system. After scaling of marker trajectories, the time courses of all marker coordinates were low-pass filtered in MATLAB, using a 4th-order Butterworth filter with a cut-off frequency of 20 Hz.
    
    The motion capture data yielded information about the animal's position and posture in each frame in a right-handed, world-fixed coordinate system (CS) with the x- and y-axes aligned with the long and traverse axes of the setup walkway, respectively, and the z-axis pointing upwards. The centre of the segment border between the 1st and 2nd abdominal segment (note that, in stick insects, the 1st abdominal segment is fused to the metathorax) was taken as origin for a thorax-fixed root CS. With regard to this root CS, all positions of the other thorax segments and the coxae were expressed in right-handed Cartesian coordinates, with the x-axis pointing rostrad within the sagittal plane, i.e., from the origin towards the head, the horizontal y-axis pointing towards the left within the horizontal body plane, and the z-axis pointing dorsad within the sagittal plane.
    
    \textit{Calculating the main body chain}: The main kinematic chain included the three thorax segments and the head. The root segment (metathorax, including the fused 1st abdominal segment) had six DoF: three translational DoF indicating the position of the body in the external coordinate frame [x0, y0, z0] and three rotational DoF, indicating roll, pitch and yaw rotation around the x0-, y0-, and z0-axis, respectively. The other three segment joints of the main body chain had two rotational DoF each, indicating pitch and yaw rotation around the segments y- and z-axes, respectively. This resulted in twelve DoF for the main chain. In four animals with 17 markers (without second mesothorax marker), the metathorax-mesothorax joint was assumed to be immobile.
    
    The rotation of the root segment with respect to the world coordinate system was determined from the axis orientations of a body-fixed root coordinate system ([xR, yR, zR] in Fig. \ref{fig:body_kinematics}B). The latter was defined by the three markers on the root segment, such that xR pointed in the direction of the main chain and zR was orthogonal to the plane defined by the three markers. The calibration images of the asymmetric side marker on the root segment yielded a bias rotation angle. Back-rotating the marker-fixed root coordinate system by this angle yielded alignment [xR, yR, zR] with the sagittal, horizontal and frontal body planes. Measures taken from calibration images were then used to calculate the origins of all connecting segments. In case of the root segment, these were the mesothorax (T2) and the hind leg coxae (R3.cox, L3.cox). Next, the vector connecting the root-T2 joint with the marker on T2 was calculated. After back-rotating this vector by its bias rotation with respect to [xR, yR, zR], as determined from calibration images, its polar coordinate angles yielded the joint rotation angles around the axes T2.z  and T2.y. The resulting T2-fixed coordinate system was used to calculate the origins of the prothorax (T1) and of the middle leg coxae. The rotation angles of the T2-T1 joint and T1-head joint, along with the remaining segment origins of the main body chain were calculated in analogy to the calculation steps taken for T2.  
    
    
\textit{Calculating the six side chains}: Each thorax segment was connected to two kinematic side chains, modelling the left and right legs (see Fig. \ref{fig:body_kinematics}, right panel, where R1 to R3 label the right front to hind legs). The side chains consisted of a coxa with three rotational DoF in the thorax-coxa joint (ThC-joint [protraction/retraction, levation/depression, supination/pronation]), the trochantero-femur (sub­sequently called femur) with one DoF in the coxa-trochanter joint (CTr-joint, [levation/ depression]), and the tibia with one DoF in the femur-tibia joint (FTi-joint, [extension/flexion]). For calculation of the leg joint angles, the first step was to determine the \glqq leg plane\grqq\,spanned by the two leg markers and the origin of the corresponding side chain. If the normal vector of this plane was expressed within the coordinate system of its connecting thorax segment, its polar coordinate angles gave the protraction/rectraction and supination/ pronation of the ThC-joint, along with the rotated z- and x-axes defining the leg plane. The sum of levation/depression in the ThC- and CTr-joints was then calculated by expressing the vector connecting the ThC-joint to the femur marker within the xz-coordinate system of the leg plane. From the known segment lengths of coxa and femur, along with the exact marker position on the femur, the relative contribution of the ThC- and CTr-joint to femoral levation could be determined by triangulation. Finally, the known femur length was used to determine the location of the FTi-joint, and the vector connecting the latter to the tibia marker was used to calculate the extension/flexion of the FTi-joint (with consideration of the bias rotation caused by the misalignment of the tibial marker and the tibial axis).   
    
    
    %S-4
    \section{Analytical Reproducibility} \label{ReX}
    %S-4
    
    
    All data files and MATLAB scripts  for analysis as listed in Fig. \ref{fig:fig2-workflow} were made available by the Biological Cybernetics group. As a result, the data and scripts are available at {\url{https://gitlab.ub.uni-bielefeld.de/conquaire/biological-cybernetics}.
Data that were not part of the reproducibility check (e.g., raw video files, fotos and data files used by the proprietary software Nexus only) will not be discussed. 
    

    
    %sss-4.2
    \subsection{Analysis pipeline, data formats and software tools} \label{TechStack}
    %sss-4.2
    As described in section \ref{duerr_methods} the research group used MATLAB for all computational data analysis and creation of plots. Accordingly, the original codebase is fully written in MATLAB. The motion data was recorded with a Vicon motion capture system, operated by the proprietary software Nexus. The reproducibility check thus started with the labelled marker trajectory data that was exported from Nexus in the C3D format. The \textit{.c3d}-files were loaded into MATLAB with the help of C3Dserver. Specific versions of MATLAB need to be installed for processing the loaded data from the C3Dserver.
    
    
    %sss-4.2.1
    \subsubsection{C3Dserver and file formats} \label{c3dServerIO}
    %sss-4.2.1
    
    The C3Dserver is a 32/64-bit C3D Software Development Kit (SDK) for Microsoft Windows\textsuperscript{\textregistered} platforms only. It simplifies C3D file programming and data access by providing the users with high-level commands to create, modify and process data. The C3Dserver can be freely downloaded and installed on all 64-bit and 32-bit versions of Microsoft Windows from XP through Windows 10 using the standard Microsoft user environment.
Data saved from the Vicon motion tracker has to be loaded into MATLAB with the help of the C3Dserver. While the server is available as 32-bit and 64-bit versions with identical C3D access functions, one can only run 32-bit applications on a 32-bit installation as the 64-bit C3Dserver DLL will not be installed on a 32-bit server.
On the other hand, if the C3Dserver is installed on a computer with a 64-bit operating system, then we can install distinct 32-bit and 64-bit DLLs, making it easier to use the C3Dserver with both 32-bit and 64-bit applications. The 64-bit DLL will be installed as \path{C:\Program Files\Common Files\Motion Lab Systems\C3Dserver\c3dserver64.dll}. The 32-bit DLL will be installed in \path{C:\Program Files (x86)\Common Files\Motion Lab Systems\C3Dserver\c3dserver.dll}.
    
    
    %SS-4.3
    \subsection{Technical Challenges and Issues} \label{TechBugs}
    %ss-4.3

Scientific research groups use a variety of file formats with various machines using standard formats to read in data and output it. Here, the captured data is stored in a \textit{.c3d}-file that can be exchanged and accessed via the C3Dserver, but it is predominantly supported to run on the Windows platform only.  The C3D file format is a public domain file format for storing motion and other 3D data recorded in various laboratory settings. The C3Dserver, whose server features include several MATLAB supporting functions that allow files to be analysed with additional MATLAB functions being written to perform operations on the data in \textit{.c3d}-file. 
 
The biggest challenge we thus faced was the requirement of the proprietary C3Dserver for data processing, analysis and visualisation that was only available for machines running the Windows operating systems. Since there was no software support for Linux to read in the motion tracking data to MATLAB, we could not recreate the full pipeline on a Linux machine. The Library is maintaining the infrastructure for research data management (RDM), hence, they would have the additional work of installing, both MATLAB and the Windows server, patching and updating them regularly, including maintaining licensed version upgrades which can get expensive over time. 
The kinematic reconstruction was achieved in MATLAB by combining marker trajectories with the body documentation. The resulting processed data, i.e., joint angle time courses, gait pattern, and velocity, were saved as another \textit{.mat}-file.

Another problem was related to the backslash used in PATHS on the Windows machine. All relative paths in the code supported Windows, which uses a backslash instead of (forward)slash on *nix machines. While analysing the MATLAB data with C3Dserver and MATLAB on Windows, this is not an issue. However, a user trying to use the MATLAB code on a *nix machine would have to replace and correct all the paths before running the code to reproduce the figures from that point onwards. 
For example: For Figure \ref{fig:compare_duerr}B the *nix user can type these code commands from the terminal after they loaded the data beforehand:

{\tiny    
\begin{verbatim}
figure; hold on
%    Trajectory of the tibia-tarsus joint of the left front leg
plot3(data.L1.tar.pos(:,1), data.L1.tar.pos(:,2), data.L1.tar.pos(:,3),'r', 'LineWidth', 2) 
%    Trajectory of the tibia-tarsus joint of the right front leg
plot3(data.R1.tar.pos(:,1), data.R1.tar.pos(:,2), data.R1.tar.pos(:,3),'g', 'LineWidth', 2)
%    Trajectory of the head
plot3(data.Hd.pos(:,1), data.Hd.pos(:,2), data.Hd.pos(:,3),'k', 'LineWidth', 2)
%    Equal aspect ratio
axis equal
\end{verbatim}
}
    
Furthermore, the most severe limitation was due to the use of proprietary software tools, like Windows-only SDK. As there was no free and open source software (FOSS) support for the SDK, it was impossible to recreate or plug into the analysis pipeline with a Linux machine.
    Since MATLAB uses Gnuplot as the plotting engine, we could pipe-in (read) the data with Octave2, an open source MATLAB clone, and plot the data.  As the plotting engine (Gnuplot) is the same for Octave and MATLAB the figure rendering is similar to the published paper. Thus, the three figures in the paper can be reproduced using FOSS toolkits in a Linux environment if the data was created beforehand with the help of the C3Dserver and MATLAB on Windows.
    
As a result of our reproduction experiment, we could reproduce the walking and climbing behaviour for those experimental runs that were committed into the corresponding GIT repository.
Figure \ref{fig:compare_duerr} shows on the left the original panel from the  paper published by Theunissen et al. \cite{Theunissen_EtAl_2015} for \textit{C. morosus}. On the right, our reproduction of the same trial is depicted. As the figure shows, asides from the rendering of the obstacle and the colouring, we could successfully reproduce the plots from the original paper.    


\begin{figure}[]
  \centering
  \includegraphics[width=12cm]{../ch2-BiologyDuerr/images/fig5-compare.png}
  \caption{\textbf{Representative trial of unrestrained walking and climbing behaviour of \textit{C. morosus} as one of the three species investigated in the original paper published by Theunissen et al. \cite{Theunissen_EtAl_2015} (Figure 3).}
        The left panel \emph{L} shows the original figure section. The right panel \emph{R} shows the movement as reproduced in the reproduction study conducted in the context of this chapter.  The A, B and C subcomponents of the diagram show the following:
        \textbf{A}: Movement of the body axis (cyan lines), head (red circles) and front legs (black lines), illustrated by superimposed stick figures every 100 ms. 
        \textbf{B}: Trajectories of the tibia-tarsus joint of left (red) and right (green) front legs, and of the head (black line) super-imposed on the setup in side and top view. Note that the caption of the original publication says ‚metathorax‘ instead of ‚head‘ at this place. This mislabelling was discovered during the replicability study. The authors apologise for this error. The mislabelling has no effect on any claims made in the original paper.
        \textbf{C}: Podogram of the gait pattern, i.e., time sequences of the alternating swing-stance-phases of all six walking legs, where each black line depicts the duration of a stance phase of one of the legs. Red and green lines mark the first stance phases on the next stair in left and right legs, respectively. 
        \textbf{L1 to L3}: left front, middle and hind leg; 
        \textbf{R1 to R3}: corresponding right legs.}
  \label{fig:compare_duerr}
\end{figure}

       
    
%S-6
\section{Conclusion}

We have described a reproducibility case study in the field of biology. We have in particular attempted to represent the main results of a study in whole-body movement analysis of three species of stick insects. The main objective of the study was to relate inter-species differences in kinematics to differences in overall morphology, including features such as leg-to-body-length ratio, which were not an obvious result of phylogenetic or ecological divergence. We have shown that we could successfully reproduce a main figure of the paper \emph{``Comparative whole-body kinematics of closely related insect species with different body morphology''} by Theunissen et al. \cite{Theunissen_EtAl_2015}. We classify this case as one of \emph{limited analytical reproducibility}. While we could reproduce the whole-body movements for a number of experimental runs that the authors provided in a GIT repository, this has only been possible by direct guidance of the authors. Further, the reproduction relies on use of commercial software, in particular MATLAB as well as the C3Dserver running on Windows only.


\FloatBarrier    
\section*{Acknowledgements}

We would like to thank Florian Paul Schmidt for uploading the files to the \textit{biological-cybernetics} repo in the Gitlab \textit{Conquaire} group. We would like to thank Lukas Biermann for helping with the reproduction of the analyses in MATLAB.


\bibliographystyle{unsrt}
{\raggedright  % group bib left align
\bibliography{ch2-BiologyDuerr}
}
% Add Bibliography to ToC
\addcontentsline{toc}{section}{Bibliography}

