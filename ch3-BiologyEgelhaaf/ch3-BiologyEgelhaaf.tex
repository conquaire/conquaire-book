\chapter{Reproducing Trajectory Analysis of Bumblebee Exploration Flights}

\label{egelhaaf_chapter}

   \chapterauthor[1]{Vineet Sharma}
   \chapterauthor[2]{Olivier Bertrand}
   \chapterauthor[2]{Jens Lindemann}
   \chapterauthor[1]{Cord Wiljes}
   \chapterauthor[2]{Martin Egelhaaf}
   \chapterauthor[1]{Philipp Cimiano}
   \begin{affils}
     \chapteraffil[1]{Semantic Computing Group, Faculty of Technology \& Cognitive Interaction Technology Excellence Center (CITEC), Bielefeld University}
     \chapteraffil[2]{Faculty of Biology, Bielefeld University}
   \end{affils} 



\section*{Abstract} \label{abstract}

This chapter describes a case study in using a combination of virtualization technology, Git as well as a continuous integration (CI) server to support sustainability of analytical pipelines. The case study was designed to reproduce one processing step in the analytical pipeline described in the paper  \emph{``Taking a goal-centered dynamic snapshot as a possibility for local homing in initially naive bumblebees''} \cite{Lobecke2018}. In this paper, the researchers report their findings regarding the exploratory flights of bumblebees in unknown territories. Trajectories were recorded using two cameras and triangulated, yielding 3D trajectories of the flights. 
The original analytical workflow was implemented in MATLAB. As a result of Conquaire, the analytical workflow could be reproduced using Python, yielding trajectories that faithfully match the original trajectories.
In Conquaire, we implemented an analytical workflow that relies on virtualization as well as on a continuous integration server. The main function of the virtualization is to preserve the computational environment so that it can be easily executed by third parties without the need to reproduce the exact computational environment nor to install any libraries. A continuous integration server was used to implement basic mechanisms for quality control over the data, leading to the discovery of some minor mistakes that could be directly corrected.
The case study has demonstrated the usefulness of using a combination of virtualization and continuous integration to support analytical reproducibility in the natural sciences, neuroethology in particular.

\section*{Keywords} \label{keywords}
Insect spatial locomotion, bumblebee flights, analytical reproducibility, virtualization, continuous integration

\section{Introduction} \label{intro}
    
Animals move in their environment in a quest for food, a mating partner, or a place to raise their offspring. The animals, therefore, need to solve spatial tasks, viz. orientating themselves, identifying and reaching a target (such as a mating partner or a food source), following habitual routes (for e.g., between their home and food sources). Even in cluttered environments, animals manage to solve these complex spatial tasks without collisions with obstacles in their path. These abilities are not only observed in vertebrates but also in insects with small brains. Indeed, flying insects can chase their partner \cite{Boeddeker2003}, learn the surroundings of their nest \cite{Robert2018,Lobecke2018}, cross cluttered environments \cite{Crall2015,Kern2012}, and follow routes \cite{Woodgate2016,Lihoreau2010}. Given the small number of nerve cells in insect brains and the limited reliability of neurons in general, extracting information required to solve navigational tasks needs to rely on extremely efficient neural mechanisms. As a consequence of millions of years of evolution, these mechanisms are tightly linked to the sophisticated locomotion and gaze strategies of insects. 

The research focus of the Neurobiology group at Bielefeld University is to elucidate the computational principles, down to the level of neurons and neural networks that generate and control visually guided behaviour in complex and cluttered environments. 
Understanding the computational principles involved in visually guided behaviour requires, first, monitoring the behaviour of the animal over long periods, and second, reconstructing the visual perception of the environment from the animal's perspective. 

The visual processing and behaviour of insects is extremely fast, and hence monitoring their behaviour and reconstructing it requires high frequency and precision recording techniques to obtain the position and orientation of the animal. The position of an animal in an environment can be accurately derived via triangulation or 3D reconstruction of high-frequency data from video recordings of the animal taken with several synchronized cameras. This method requires a precise orientation and positioning of the camera, as well as a correction of potential distortions due to the lens, and an accurate detection of the position and orientation of the insect on the camera (obtained by feature extraction). However, no tracking software is error-free, and thus, the recording even after manual reviewing may contain errors (especially for extended recordings, e.g. of several 10,000 frames) that need to be automatically post-processed by a later processing stage.

Lobecke et al. \cite{Lobecke2018} recorded the behaviour of naive bumblebees exiting their nest for the first time. This behaviour can last for several minutes, and the monitoring of the animal's behaviour resulted in the collection of several thousand images on which the bumblebees' positions were automatically tracked and manually reviewed. The orientations of the bumblebees during their learning flights were obtained from the recorded positions using the Camera Calibration Toolbox from MATLAB \cite{caltoolbox}). 

In this chapter, we discuss a case study in applying a combination of continuous integration principles, virtualization and Git to support reproducibility of one computational step in the experimental pipeline described by Lobecke et al. \cite{Lobecke2018}. Our main motivation for this case study is to develop best practices that support the execution of the original analytical workflow by third parties. For this reason, we explore how virtualization technology can be used to create a reproducible computational environment that can be directly executed without the need to install software. An approach based on virtualization prevents problems related to broken dependencies due to later non-availability of the required version of software and packages. In addition to using virtualization, we make use of an integration server to specify and execute a number of integrity tests that ensure validity of the data. 

The structure of this chapter is as follows: in the following section \ref{methods_egelhaaf}, we describe how the data in the original study by Lobecke et al. was collected. In section \ref{analytical_architecture}, we describe the technical environment we have set up to preserve the computational environment and thus ensure executability of the analytical workflow. We also describe how we have used continuous integration (CI) principles to implement a set of quality checks and integrity tests that ensure the validity of the data.


%\subsection{Main publication result reproduced within Conquaire}
% 
%Within Conquaire, we aimed at reproducing the 3D trajectories from the 2D trajectories by using an alternative calibration technique [Bouget et al]. Moreover, we developed automated techniques to warn the user of possible mistracking or reconstruction. We will highlight the necessity to not only publish the result of the triangulation but also camera characteristics (such frame size, frame rate), and the reviewed tracking of the bumblebee on the camera. 
    
   
\section{Experiment settings and data acquisition pipeline}

\label{methods_egelhaaf}
    
The behaviour of naive bumblebees was recorded with two cameras (Falcon2 3M, Teldyne Dalsa, Inc) at 148fps, an exposure time of 1/1000s and a spatial resolution of 2048x2048 px. The focal lens of the cameras was 8mm, and the physical pixel size was 6 $\mu$m.
The behaviour of bumblebees was continuously monitored for several hours on a hard disk array using the software Marathon Pro (GS Vitec, Germany). Relevant sequences of learning flights were stored as 8-bit jpeg images for the flight analyses. From the series of images, the position of the bumblebee on the image was obtained by segmenting the image into background and foreground and fitting an ellipse around the foreground (the bumblebee) by using the software ivTrace\cite{Lindemann2005}\footnote{\url{https://opensource.cit-ec.de/projects/ivtools}}). 

After this automated procedure, the position and orientation of the bumblebee on the images were manually reviewed and potential errors were corrected by watching the video frame by frame and using the software ivTrace. In a parallel step, the Camera Calibration Toolbox for MATLAB by Jean-Yves Bouguet\footnote{\url{http://www.vision.caltech.edu/bouguetj/calib_doc/}} was used for the camera calibration and the 3D stereo triangulation. A checkerboard pattern (5 cm per square) was used for the calibration and the difference between checkerboard points recorded by the camera and checkerboard points reprojected to the images from their triangulated 3D positions was determined. The average position error for the top and the side camera were 0.11 and 0.09 px, respectively. 
    
Lobecke et al. \cite{Lobecke2018} reported that the first learning flights of bumblebees are highly variable and depend on the recorded individual. The learning flight was recorded along a prolonged time-span and at a high spatio-temporal resolution. The bumblebees' flight positions and orientations were then reconstructed by using triangulation from two synchronized cameras. Fig. \ref{fig:original_workflow} depicts the computational workflow of the calibration to triangulation process. Fig. \ref{fig:trajectory_3d} depicts an example of a trajectory of a bumblebee flight. For more detailed depiction, see \cite{Lobecke2018}.

All data files, MATLAB and Python scripts for analysis as listed in Fig. \ref{fig:original_workflow} were made available by the Neurobiology group. 
%As a result, the data and scripts are made available \footnote{\url{https://GitLab.ub.uni-bielefeld.de/olivier.bertrand/tra3dpy}}. 
The XML-file (Fig. \ref{fig:xml_format}) contains parameters of the camera that were used for recording the bee flight movement. They are used in the triangulation process to calculate trajectories using two \emph{tra format} files. The dataset is the basis for a publication by Lobecke et al. (2018) \cite{Lobecke2018}. 
The tra files (Fig. \ref{fig:tra_format}) contain the trajectory values in 2D format from two cameras, one located on top and the other located on the side of the bee.
The MATLAB file format contains resulting trajectory information in 3D format. 


% Cemplex trajectory depiction
%\begin{figure}[!ht]
%  \includegraphics[width=\linewidth]{images/Figure1_Lobecke.png}
%  \caption{Flight trajectories of two flights of three different bumblebees seen from above and from one side Three example trajectories out of the 21 first departure flights analysed. Black circles in the top view (Ai-Ci) and grey rectangles in the side view (Aii – Cii): cylinders; blue circle in top view: Hole connected to the nest; coloured lines indicate the orientation of the bee’s body long axis every 20.27 ms; end of lines mark head position; sequence of head positions defines trajectory. Trajectories colour-coded with time: Dark blue beginning of flight, dark red end of flight. Axes scales are given in mm. Coordinates ‘0.0’ represent the centre of the flight arena. (Adapted from Lobecke et al. \cite{Lobecke2018})}
%  \label{fig:trajectories}
%\end{figure}

\begin{figure}[]
\centering
  %\includegraphics[width=0.4\linewidth]{images/original_workflow.jpg}
  \includegraphics[width=0.4\linewidth]{images/Fig4_1_NEU.png}
  \caption{Procedure to calculate the trajectories of bumblebee flights, original procedure as described in Lobecke et al. \cite{Lobecke2018}}
  \label{fig:original_workflow}
\end{figure}

\begin{figure}[]
\centering
  \includegraphics[width=\linewidth]{images/trajectories_sample_matched.png}
  \caption{Example trajectory of a bumblebee flight, seen in 3D (cf. Lobecke et al. \cite{Lobecke2018})}
  \label{fig:trajectory_3d}
\end{figure}

%Flight trajectories of first flights of three different bumblebees seen from above and from one side Three example trajectories out of the 21 first departure flights analyzed. Black circles in the top view (Ai-Ci) and grey rectangles in the side view (Aii – Cii): cylinders; blue circle in top view: Hole connected to the nest; coloured lines indicate the orientation of the bee’s body long axis every 20.27 ms; end of lines mark head position; sequence of head positions defines trajectory. Trajectories colour-coded with time: Dark blue beginning of flight, dark red end of flight. Axes scales are given in mm. Coordinates ‘0.0’ represent the centre of the flight arena.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% Computational Reproducibility %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Computational Environment for Reproducibility}

\label{analytical_architecture}

In this case study, we set up a computational environment that builds on three key components to support 3rd party execution of the analytical pipeline for computing the 3D trajectories:

\begin{itemize}
\item Git Repository: The original data and the scripts to compute 3D trajectories from the 2D data of the two cameras were uploaded to a Git repository. The benefit of using Git is that data and scripts are stored in a versioned fashion so that particular versions of data and scripts can be referenced. Further, the data is backuped.
\item Virtualization: We rely on virtualization technology to create a virtual image of the computational environment that can be shared and executed on any machine that runs the same virtualization software. In our case, we rely on VMWare.
\item Continuous Integration: We deploy a continuous integration server that pulls the data and scripts from the Git repository, builds the analytical pipeline, and executes a number of integrity tests on the data.
\end{itemize}  

In the following, we describe the virtualization and continuous integration approach in more detail. Before, however, we briefly describe how the original MATLAB code that was used in the original experiment was migrated to an open source programming language, Python in particular.

%The aim of our computational reproducibility experiment was to reproduce the bumblebee flight trajectories, as depicted in Fig. \ref{fig:trajectory_3d}, which were calculated with Matlab, with a new analysis library written in Python. Overall, 18 individual flights were tested.

\subsection{Software Migration}

The original code used in the study carried out by Lobecke et al. was written using the commercial software MATLAB. As part of Conquaire, the scripts were ported to the open source programming language Python. Some data files remained in MATLAB format, which did not constitute a problem as Python's scipy library can be used to read in MATLAB files.
The resulting Python code is available in a shared GitLab repository\footnote{\url{https://GitLab.ub.uni-bielefeld.de/olivier.bertrand/tra3dpy}}. 
%The analysis code were shared in a GitLab repository. The commands used to build, test and deploy the toolbox was shared. The codebase was run on a Linux distribution, it could be executed and built successfully in Ubuntu 16.04 distribution by installing Anaconda distribution 3.5.
%The toolbox was designed following best practices of software development and thus the entire toolbox could be easily built when the code was shared. 
The Python script reads the position of the bees from the two cameras, performs the triangulation for the two camera images and produces the 3D trajectories as output. Note, that the reconstruction of the camera calibration from the data as depicted in Fig. \ref{fig:trajectory_3d} was only necessary for reproduction purposes. For future data, the calibration parameters for the python scripts would also be generated from a checkerboard calibration processes.

Using this Python script, we could successfully reproduce the 3D trajectories from the original experiment. Fig. \ref{fig:trajectory_2d} plots the 2D projection of the 3D trajectories computed by the original MATLAB workflow in comparison to the Python-based workflow. One can appreciate that the deviations are minor and barely visible. A statistical analysis of the differences for all 18 investigated flight experiments is shown in Fig. \ref{fig:comparison}. The average error along x- and y-axis is a maximum of 0.024 mm and is much smaller than the maximum error of measurement and therefore negligible. In contrast, the average error along the z-axis is larger (0.3 mm).
%, which is as expected as the measurement error along the z-axis is also larger and it requires a more complex calculation (probably due to the position of the camera that were not at 90 degrees angle from each other.). 
However, the differences are clearly small and within an acceptable range. 

\begin{figure}[t]
  %\includegraphics[width=\linewidth]{images/original_reproduced_workflow.jpg}
  \includegraphics[width=\linewidth]{images/Fig4_3_NEU.png}
    \caption{Procedure to obtain the trajectories of bumblebees. In shaded gray: The original procedure followed in Lobecke et al. \cite{Lobecke2018}. In shaded green: the reproduced and adapted procedure. In parenthesis, the software/tools used to accomplish the task.}
  \label{fig:trajectory_3d}
\end{figure}


\begin{figure}[t]
  \includegraphics[width=\linewidth]{images/trajectories_sample_matched_2D.png}
  \caption{A close magnified snapshot displaying comparing the 2D projections to x- and z-axes of the 3D trajectories computed by the MATLAB analytical workflow (red) and the Python-based workflow (green).}
  \label{fig:trajectory_2d}
\end{figure}


\begin{figure}[H]
   \centering
      %\subfloat[CAPTION]{BILDERCODE}\qquad
      \includegraphics[width=0.3\linewidth]{images/trajectory_distribution_x.png} \includegraphics[width=0.3\linewidth]{images/trajectory_distribution_y.png} \includegraphics[width=0.3\linewidth]{images/trajectory_distribution_z.png}
   \caption{Distribution of differences between original MATLAB and new Python calculation for the three dimensions, x, y and z respectively}
   \label{fig:comparison}
\end{figure}

\subsection{Virtualization}

A virtual machine was set up with the necessary libraries and dependencies required to run the toolbox. A linux-based virtual environment was created using VMWare.  The virtual machine was provided with 2GB RAM and 50GB of storage. The CI server Jenkins was installed and the Python environment needed to execute the Python tool mentioned above was setup. In particular, Python version 3.4 was installed.
The benefit of the virtualization is that the computational workflow can be executed by a third party without any need for installing operating systems, software nor libraries except for setting up a machine that runs VMWare and that supports execution of the virtual image. Thus, the party interested in running the computational workflow does not have to take care of installing any packages with the correct version. Further, the workflow can be executed in spite of the specific version of the libraries on which the script depends not being available anymore.

%\begin{figure}[H]
%  \centering
%  \includegraphics[width=0.7\linewidth]{images/Jenkins-Pipeline.jpg}
%  \caption{Flow Chart of Jenkins Continuous Integration pipeline.}
%  \label{fig:Jenkins-Pipeline}
%\end{figure}

%To further ease dependencies and provide support for the researchers, Jenkins CI Pipeline \footnote{\url{https://jenkins.io/doc/book/pipeline/}}, an open source automation server to reliably build, test, and deploy software, was installed on the virtual machine. Jenkins CI achieves deployment of the toolbox in a repeatable and reliable way by building the toolbox whenever a change is made in its Git repository, thus solving the need to manual build the project. The different stages of the pipeline are displayed in the Fig. \ref{fig:Jenkins-Pipeline}).


\subsection{Continuous Integration supporting quality control}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\linewidth]{images/Jenkins-Pipeline.jpg}
  \caption{Flow Chart of Jenkins Continuous Integration pipeline.}
  \label{fig:Jenkins-Pipeline}
\end{figure}

As mentioned above, a Jenkins server was installed and deployed on the virtual machine. The Jenkins server is used to automate the process of checking out the toolbox from the Git repository and deploying the analytical pipeline in the local (virtual) machine. It allows to deploy the toolbox in a repeatable and reliable way involving automated testing. The CI workflow has been implemented in such a way that it continuously checks the Git repository for new changes and executes the whole pipeline every time the data and/or scripts have been updated. The workflow also installs all the necessary Python libraries using the pip package manager. 
The whole pipeline is depicted in Figure \ref{fig:Jenkins-Pipeline}. After starting the Jenkins Server and starting the workflow, the data and scripts are checked out from the Git repository. Then, the necessary Python libraries are installed on the virtual machine and the project is build. A number of unit tests are performed on the software. Then, a number of data validation tests are executed and the test results are stored in a log. When all tests are passed, the toolbox is run on the data and the results of the analysis are stored.

Data validation tests were written for the three types of files: 

\begin{figure}[]
  \centering
  \includegraphics[width=0.7\linewidth]{images/xml_format.jpg}
  \caption{Camera calibration data in XML format}
  \label{fig:xml_format}
\end{figure}

\begin{figure}[t]
  \centering
  \includegraphics[width=0.7\linewidth]{images/tra_format.jpg}
  \caption{The tra format. The  rows are in the following format: frame number, x, y, orientation, roundness, size}
  \label{fig:tra_format}
\end{figure}

\begin{itemize}
\item XML file: The XML file describes parameters of the camera used when recording the bees' flight movements (see \ref{fig:xml_format} for a sample). We implemented a parser that checks the syntactic well-formedness of the XML file. In addition, we implemented a set of basic tests checking that the x- and y-position of the center of the camera is within acceptable ranges. The test succeeds if the center of both cameras is less than half of the size of the camera. Finally, we wrote a test to check that the focal length parameter of the camera is within acceptable ranges. 

\item tra files: The tra files contain 2D trajectory values of the bees' flights as recorded by the two cameras. A set of unit tests was implemented to check that there are no empty values for any row/column as well as that each value is of numeric type. In addition, we implemented checks to verify that the values are within acceptable ranges as specified for each column. A sample of the data is shown in Figure \ref{fig:tra_format}.

\item MATLAB files: The MATLAB files contain the 3D trajectories as calculated from the 2D files using a triangulation mechanism described by Lobecke et al. \cite{Lobecke2018}. We implemented a test that computes the distance between any subsequent 3D data points and computes the bumblebee's speed from the distance and frames per second as recorded by the cameras. The test is passed if the speed is below the maximum of 10m/s.
\end{itemize}
 


%All data files, Matlab and Python scripts for analysis as listed in Fig. \ref{fig:original_workflow} were made available by the Neurobiology group. 
%%As a result, the data and scripts are made available \footnote{\url{https://GitLab.ub.uni-bielefeld.de/olivier.bertrand/tra3dpy}}. 
%The xml-file (Fig. \ref{fig:xml_format}) contains parameters of the camera that were used for recording the bee flight movement. They are used in the triangulation process to calculate trajectories using two \emph{tra format} files. The dataset had been partially already published by Lobecke et al. (2018) \cite{Lobecke2018}. 
%The tra files (Fig. \ref{fig:tra_format}) contain the trajectory values in 2D format from two cameras, one located on top and the other located on the side of the bee (Fig. \ref{fig:tra_format}). 
%The Matlab file format contains complete trajectory information in 3D format (Fig. \ref{fig:mat_format}). 
%
%The data files were subjected to various types of semantic and syntactic checks. The checks carried out for each type of files are as follows:
%
%\begin{description}
%\item [XML format] well-formedness (format of XML), missing data, center of camera slightly more or less than half of its size, focal length of camera with a plausile range(8 mm) Fig. 
%\item [tra format] data type (numeric) and range of values in different columns
%\item [Matlab format] bee flight speed within a plausible range (0 - 10 m/s) 
%\end{description}
%
%\textbf{pci: This needs to be fixed...}

These tests were intended to validate the data by discovering potential errors. The XML file with the camera parameters passed all the tests. Our validation scripts highlighted that some rows in the tra files had missing values and that some rows had 11 (instead of 6) values. 
In the case of the MATLAB files, some tests were not passed as for a number of data points the bumblebee's flight speed was observed to be out of the possible range (Fig. \ref{fig:bee_flight_in_range} and Fig. \ref{fig:bee_flight_out_of_range}). Overall, this validation helped the researchers to discover small errors in the data and correct them.

\begin{figure}[t]
\centering
\includegraphics[scale=0.7]{images/bee_flight_in_range.png}
\caption{Flight speed for a bee which is well within the range as defined by the researcher (below 10m/s).}
\label{fig:bee_flight_in_range}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[scale=0.7]{images/bee_flight_out_of_range.png}
\caption{Flight speed for a bee in which an error in the data was found. The erroneous speed was above 150 m/s, which is far outside the acceptable range.}
\label{fig:bee_flight_out_of_range}
\end{figure}





%\begin{figure}[]
%  \centering
%  \includegraphics[width=0.7\linewidth]{images/mat_format.jpg}
%  \caption{The mat format. The rows are in the following format: x, y, z, orientation.}
%  \label{fig:mat_format}
%\end{figure}

%Fig. \ref{fig:xml_format}: The tags are in the following format:\\ 
%ncameras: number of cameras,
%intrinsic\_matrix\_0 : matrix containing focal length and center of the camera (with postfix indicating the camera number),
%rows and columns: represent the number of rows and columns in the matrix, data: contains values of the data matrix, distortion\_matrix\_0 : describes the lens distortion of camera (with postfix indicating the camera number), pose\_matrix\_0 :describe the position orientation of the camera (with postfix indicating the camera number).


  
   

%\subsection{Reproducing experiment results using another programming language}
    
%We developed the Toolbox using Python 3.5 programming language along with Anaconda distribution. The operating system used to develop and execute was Linux (Ubuntu). Source code management was carried out using Git version control system on GitLab.

%We used the following Python libraries:\\
%    
%    \begin{enumerate}
%        \item opencv-python: It provides computational effciency and with a strong focus on real-time applications taking advantage of multi-core processing.
%        \item sphinx: It is a tool used to create intelligent and beautiful documentations.
%        \item numpy: It is a package for scientific computing.
%        \item pandas: It is a library which provides high-performance, easy to use data
%        structures and data analysis.
%        \item matplotlib: It is a library for high quality 2D plotting.
%        \item scipy: It is an ecosystem for science, mathematics, engineering.
%        \item tables: It is a library used for maintaining and coping up with huge datasets.
%        \item pillow: It is an imaging library.
%        \item flake8: It is a library which verifies coding standard.
%        \item ipython: It is an interactive command line terminal for Python.
%        \item tox: It is a tool to automate and standardize testing.
%    \end{enumerate}








%\begin{figure}[]
%\centering
%\begin{subfigure}[b]{0.3\textwidth}
%   \centering
%  \includegraphics[width=\linewidth]{images/trajectory_distribution_x.png}
%  \caption{Nr. 1}
%  \label{fig:mouse}
%\end{subfigure}
%\begin{subfigure}[b]{0.3\textwidth}
%   \centering
%  \includegraphics[width=\linewidth]{images/trajectory_distribution_y.png}
%  \caption{Bild Nr. 2}
%  \label{fig:cat}
%\end{subfigure}
%\begin{subfigure}[b]{0.3\textwidth}
%  \centering
%  \includegraphics[width=\linewidth]{images/trajectory_distribution_z.png}
%  \caption{Bild Nr. 3}
%  \label{fig:dog}
%\end{subfigure}
%   \caption{Distribution of differences between original Matlab and new Python calculation for the three dimensions, x, y and z respectively}   
%   \label{fig:comparison}
%\end{figure}




%A continuous integration pipeline was set up so that whenever any changes are committed into the repository, the Conquaire server starts by creating a python virtual environment and installing the necessary libraries required to execute the toolbox project. After that, building and installing a module distribution is carried out. Next, the unit test cases and data test cases are executed. When the tests pass successfully, the toolbox is deployed in the machine where it can be used locally. On failure of the tests, the pipeline stops and the logs are displayed in the console.
 

%The syntactic data tests reported some missing rows but they were already known to the researchers and were removed when analysis was done. The semantic data checks were also successful as the data was well within range but for one case the bee flight speed, the data was could not be matched due to manual calibration error and is being investigated by the researchers. 


\section{Conclusion}

We have described a case study in applying a combination of continuous integration principles, virtualization and Git to support reproducibility of one computational step within an experiment in neurobiology studying the first flights of bumblebees. Git supports the versioned storage of data and scripts so that we can refer back to any version of the data if needed. Virtualization technology allows to preserve the computational environment in order to avoid a situation in which the software can not run any more due to broken dependencies, non-availability of the particular version of a required software, etc. Third party researchers can re-run the computational procedure by merely installing the image of the virtual machine, without having to install any further software or having to built it.
A continuous integration server has been deployed on the virtual machine to automatically pull the most recent version of the data on the repository, build the computational pipeline and run a number of tests that check the well-formedness of the data. 

In the specific use case considered, the use of virtualization and continuous integration might be considered an overkill as the processing scripts in Python that calculate the 3D trajectories have a limited complexity. The quality tests implemented are also rather simple. 
Yet, our goal has been to understand the potential of using virtualization and continuous integration, also with respect to more complex cases and experimental environments in which more complex software artifacts and analytical pipelines are involved. 
In the specific case study considered, we could successfully re-run one computational step from the experimental settings described in the paper  \emph{``Taking a goal-centered dynamic snapshot as a possibility for local homing in initially naive bumblebees''} \cite{Lobecke2018}. In particular, we could rerun the step that calculates and visualizes the trajectories of bumblebees. In this sense we could reproduce a key step in the analysis of the recorded flights.
 
A drawback of our proposed architecture and combination of virtualization, continuous integration and Git is that the data resides on a Git repository and is pulled every time the computational pipeline is deployed and tested by the continuous integration server. While this allows to pull the most recent version of data and scripts, in our experience once the data and scripts are final, they are typically not modified so that a static inclusion of the data and scripts in the virtual machine would be sufficient. The dependency on a Git repository introduces a dependency that can potentially break if the Git server is not hosted anymore.
In future work, the potential and benefits of using virtualization in combination with a continuous integration server should be further investigated on additional use cases. Especially, using the CI pipeline for continuous quality control on newly recorded data in follow-up projects would be highly beneficial for neuroethological research.


%The research project proved execellent computational reproducibility. Migrating the analysis pipeline from Matlab to Python resulted in flight trajectories that matched the original results. Differences were within calculation precision and so minor that they do not affect the interpretation, i.e. the research results. 
%
%Overall, the research project followed excellent standards in following best practives in software development and documentation, so their code could be easily executed. In addition, successful migration to the open source programming Python illustrates that exchanging a commercial software (Matlab) by an Spen Source software (Python) is possible and can benefit the reproducibility of research.    
%
%Analysis scripts and data were made available under open licenses to further stregthen re-use and reproducibility of research.
 
%Upon implementation of the CI pipeline, the order of execution required to deploy the toolbox successful was assured. The data tests could provide additional checks on the dataset to assure that the data is correct but could also discover some small anomalies.
       
%\paragraph{Recommendations for future work}
%
%Since the toolbox is developed with a vision towards re-usability and easier understanding, it would be best to continue the same. To make it more robust, more automated unit tests development should be continued and there can be more data quality tests that can be implemented to make the toolbox more robust. The team is already working out ways to continue to use continuous integration for easier builds.
%    
%In addition, some syntactic data quality tests like completeness, correctness, missing of values in the data could also be implemented and semantic check like validation of bee flight speed, range of values of the data.


%A detailed description of the work was presented in .


\FloatBarrier
%\bibliographystyle{plain}
\bibliographystyle{unsrt}
%\bibliographystyle{alpha}
%\bibliographystyle{apacite}
{\raggedright  % group bib left align
\bibliography{ch3-BiologyEgelhaaf}
}

% Add Bibliography to ToC
\addcontentsline{toc}{section}{Bibliography}
